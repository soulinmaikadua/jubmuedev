import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isShowPassword = false;

  _handleEye() {
    setState(() {
      _isShowPassword = !_isShowPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(100, 100, 100, 50),
              child: Image.asset("assets/images/jaocandev_logo_2022.png"),
            ),
            Text(
              "ເຂົ້າສູລະບົບ",
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                prefixIcon: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    "+85620",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                hintText: "ເບີໂທ",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            SizedBox(height: 20),
            TextFormField(
              obscureText: _isShowPassword,
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  onPressed: () {
                    _handleEye();
                  },
                  icon: Icon(Icons.visibility),
                ),
                hintText: "ລະຫັດຜ່ານ",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            SizedBox(height: 20),
            Container(
              width: double.infinity,
              height: 60,
              child: ElevatedButton(
                onPressed: () {},
                child: Text(
                  "ເຂົ້າສຸລະບົບ",
                  style: TextStyle(fontSize: 18.0),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
