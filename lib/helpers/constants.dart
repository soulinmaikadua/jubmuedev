import 'package:flutter/material.dart';

const JAOCANDEV_URL =
    "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/jaocandev2.png";

Widget defaultWidth5 = const SizedBox(width: 5);
Widget defaultHeight5 = const SizedBox(height: 5);
Widget defaultHeight10 = const SizedBox(height: 10);
Widget defaultHeight20 = const SizedBox(height: 20);
