import 'package:intl/intl.dart';

final formatCurrency = NumberFormat("#,###", "en_US");

String customCurrency(int money) {
  if (money != 0) {
    return formatCurrency.format(money);
  } else {
    return "0";
  }
}

String customDateFormat(String date) {
  if (date != "") {
    DateTime now = DateTime.parse(date);
    String formattedDate = DateFormat('dd/MM/yyyy, kk:mm').format(now);
    return formattedDate;
  } else {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('dd/MM/yyyy, kk:mm').format(now);
    return formattedDate;
  }
}

String customTime(String date) {
  if (date != "") {
    DateTime now = DateTime.parse(date);
    String formattedDate = DateFormat('kk:mm a').format(now);
    return formattedDate;
  } else {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('kk:mm a').format(now);
    return formattedDate;
  }
}
