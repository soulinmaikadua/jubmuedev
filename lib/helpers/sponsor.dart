var sponsors = [
  {
    "name": "Lao Telacom",
    "image":
        "https://halberdbastion.com/sites/default/files/styles/medium/public/2018-02/LTC-Lao-Telecom-logo.png?itok=RXThIwJF",
    "supportAs": "Diamond",
  },
  {
    "name": "Foodpanda",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/foodpanda.png",
    "supportAs": "Diamond",
  },
  {
    "name": "ນຳ້ດື່ມ ຫົວເສືອ",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/tigerhead.png",
    "supportAs": "Diamond",
  },
  {
    "name": "Khoomue",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/khoomue.png",
    "supportAs": "Platinum",
  },
  {
    "name": "Welnance",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/welnance-logo-1.jpg",
    "supportAs": "Platinum",
  },
  {
    "name": "NCC Group",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/ncc.png",
    "supportAs": "Platinum",
  },
  {
    "name": "BDB",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/bdb.png",
    "supportAs": "Gold",
  },
  {
    "name": "IT Corner",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/logo+ITcorner.png",
    "supportAs": "Silver",
  },
  {
    "name": "Wayha",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/wayha.png",
    "supportAs": "Silver",
  },
  {
    "name": "Kafepa",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/kafapa.jpeg",
    "supportAs": "Silver",
  },
  {
    "name": "Makerbox",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/TheMakerBox-Logo-2021-03-30.png",
    "supportAs": "Silver",
  },
  {
    "name": "Hal Tech",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/hal+tech.png",
    "supportAs": "Silver",
  },
  {
    "name": "Z.com",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/z.com.png",
    "supportAs": "Silver",
  },
  {
    "name": "YesPls",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/sponsors/YesPls+Logo.jpg",
    "supportAs": "Silver",
  }
];
