const lab2 = [
  {
    "from": "09:25",
    "to": "10:05",
    "title": "ວິທີຂຽນໂຄດໃຫ້ເຂົ້າໃຈງ່າຍ",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/speakers/soulin+mkd.JPG",
    "guestFirstName": "ສຸລີນ",
    "guestLastName": "ໄມກ່າດົ່ວ",
    "guestFrom": "Lailaolab",
  },
  {
    "from": "15:25",
    "to": "16:10",
    "title": "ການ Scale Server ດ້ວຍ Terraform",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/speakers/souksavanh.jpeg",
    "guestFirstName": "ສຸກສະວັນ",
    "guestLastName": "ເພັດລາສີ",
    "guestFrom": "Lailaolab",
  },
];
