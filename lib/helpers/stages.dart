var stages = [
  {
    "from": "09:25",
    "to": "09:55",
    "title": "ແຊຣປະສົບການ ແລະ ການເຮັດວຽກກັບບໍລິສັດລະດັບໂລກ",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/speakers/vilaphon.png",
    "guestFirstName": "ທ່ານ ວິລະພົນ",
    "guestLastName": "ຈັນທະລາວົງ",
    "guestFrom": "Lao IT Dev Co., Ltd",
  },
  {
    "from": "17:20",
    "to": "17:30",
    "title": "ກ່າວ​ປິດ​ງານ",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/speakers/tock.png",
    "guestFirstName": "ທ່ານ ນາງ ກັນລະຍາ",
    "guestLastName": "ພົມມະສັກ",
    "guestFrom": "Lailaolab",
  },
];
