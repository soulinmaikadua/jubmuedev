var lab1 = [
  {
    "from": "09:25",
    "to": "10:05",
    "title": "ການກະກຽມກ່ອນການພັດທະນາລະບົບ",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/speakers/kuang.jpeg",
    "guestFirstName": "ໄຊວິລິຍະ",
    "guestLastName": "ວັນນະວົງ",
    "guestFrom": "Lailaolab",
  },
  {
    "from": "10:10",
    "to": "10:55",
    "title": "ການສ້າງແອັບ",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/speakers/joy+xaiyavong.jpeg",
    "guestFirstName": "ມັດທຸຈັນ",
    "guestLastName": "ໄຊຍະວົງ",
    "guestFrom": "Lailaolab",
  },
  {
    "from": "13:00",
    "to": "13:40",
    "title": "Design System in Figma",
    "image":
        "https://jaocandev-bucket.s3.ap-southeast-1.amazonaws.com/jaocandev2/speakers/goft.jpeg",
    "guestFirstName": "ສຸກທະວີ",
    "guestLastName": "ຖາຊານົນ",
    "guestFrom": "Lailaolab",
  },
];
