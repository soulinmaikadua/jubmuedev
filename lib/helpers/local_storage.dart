import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  /// save String shared preferences
  static saveData(key, value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  /// save Boolean to shared preferences
  static saveBool(key, value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
  }

  /// read String from shared preferences
  static readData(key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(key) ?? "";
    return stringValue;
  }

  /// read Boolean from shared preferences
  static readBool(key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool stringValue = prefs.getBool(key) ?? false;
    return stringValue;
  }

  /// read clear all data from shared preferences
  static clearData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  /// read remove single key from shared preferences
  static removeKey(key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }
}
